# __RPG Characters__

## __Overview__

RPG Characters (Heroes) is a console application resembling the features and relationships typical of a role playing game. It is designed with a view of extendability and scalability. Various OOP concepts such as SOLID principles and design patterns are applied.

## __Description__

There are three characters - _Warriors_, _Rangers_ and _Mages_. Each has attributes _Health_, _Strength_, _Dexterity_ and _Intelligence_. They can equip themselves with _Weapons_ (_Melee_, _Range_ or _Magic_) or _Armor_ (_Cloth_, _Leather_ or _Plate_), where the latter can be placed on different body parts - _Head_, _Body_ or _Legs_.

Both characters and equipment have _levels_ that influence various attributes. In addition, different character attributes may influence the effect of any equipment item they hold, whereas the placement of armor also determines it's protection efficacy.

## __Sample Runs__


* Create a _WARRIOR_ and simulate how a _**change in experience**_ affects its stats
<table>
<thead>
<tr>
<td align="center">Description</td>
<td align="center">Sample code</td>
<td align="center">Sample output</td>
</tr>
</thead>
<tbody>
<tr>
<td>

A _Hero_ starts at _Level 1_ with _0_ experience points (_XP_) and its initial attributes of its type. A _Warrior_, for example, starts with the follwoing (_Health_ - HP, _Strength_ - Str, _Dexterity_ - Dex and _Intelligence_ - Int):
- HP: 150
- Str: 10
- Dex: 3
- Int: 1

Each additional level in the game adds some points. In the case of a _Warrior_, each step up in level increases the stats as follows:
- HP: +30
- Str: +5
- Dex: +2
- Int: +1

Furthermore, to get to _Level 2_, a hero needs to gain 100 XP. With each level, the experience threshold to reach the next level increases by 10% (rounded down).

So, if, for example, a _Warrior_ **gains 343 _XP_** from the start, it will **reach _Level 4_** (100 + 110 + 121 = 331) and will have to gain **121 _XP_ to reach _Level 5_** (331 + 133 - 343)

In a real game, the change in _XP_ could be a result from interaction with other players and/or other game elemets, tokens, etc.

_NB_: notice how setting experience back to 0 correctly reverts the stats

</td>
<td>

```java
HeroFactory heroFactory = new HeroFactory();

Hero warrior = heroFactory
    .createHero(HeroType.WARRIOR, "Mammothbolt");
warrior.printStats();

int xp = 343;
warrior.setXp(xp);
System.out.println("Set XP to " + xp + "\n");
warrior.printStats();

int xpRevert = 0;
warrior.setXp(xpRevert);
System.out.println("Set XP to " + xpRevert
     + " reverts all the stats\n");
warrior.printStats();
```

</td>
<td>

```java
Details for Warrior: Mammothbolt
HP:  150
Str: 10
Dex: 3
Int: 1
Lvl: 1
Equipped with:   
Damage of attack: 0
XP to next:      100

Set XP to 343

Details for Warrior: Mammothbolt
HP:  270
Str: 30
Dex: 11
Int: 5
Lvl: 4
Equipped with:   
Damage of attack: 0
XP to next:      121

Set XP to 0 reverts the stats

Details for Warrior: Mammothbolt
HP:  150
Str: 10
Dex: 3
Int: 1
Lvl: 1
Equipped with:   
Damage of attack: 0
XP to next:      100
```

</td>
</tr>
</tbody>
</table>    

<br>

* Create a _RANGER_ and simulate how a _**change in level**_ affects its stats
<table>
<thead>
<tr>
<td align="center">Description</td>
<td align="center">Sample code</td>
<td align="center">Sample output</td>
</tr>
</thead>
<tbody>
<tr>
<td>

Another scenario could be the defeat of the enemy, which would take the character straight to another level. To exemplify this scenario, let's take another character - _Ranger_. Like all characters, it starts at _Level 1_ with initial attributes for its type:
- HP: 120
- Str: 5
- Dex: 10
- Int: 2

Each additional level increases the stats for a _Ranger_ as follows:
- HP: +20
- Str: +2
- Dex: +5
- Int: +1

So, if a _Ranger_ reaches, say, **_Level 6_**, its stats will increase six-fold the rate per level and result in:

- HP: 120 + 6 * 20 = **240**
- Str: 5 + 6 * 2 = **17**
- Dex: 10 + 6 * 5 = **40**
- Int: 2 + 6 * 1 = **8**

And it would require to gain **161 _XP_** (100 * 1.1^5 - rounded down) **to reach _Level 7_**.

_NB_: notice how setting level back to 0 correctly reverts the stats

</td>
<td>

```java
Hero ranger = heroFactory
    .createHero(HeroType.RANGER, "Quandary Crow");
ranger.printStats();

int level = 6;
ranger.setLevel(level);
System.out.println("Set level to " + level + "\n");
ranger.printStats();

int levelRevert = 1;
ranger.setLevel(levelRevert);
System.out.println("Set level back to "
     + levelRevert + " reverts all the stats\n");
ranger.printStats();
```

</td>
<td>

```java
Details for Ranger: Quandary Crow
HP:  120
Str: 5
Dex: 10
Int: 2
Lvl: 1
Equipped with:   
Damage of attack: 0
XP to next:      100

Set level to 6

Details for Ranger: Quandary Crow
HP:  240
Str: 17
Dex: 40
Int: 8
Lvl: 6
Equipped with:   
Damage of attack: 0
XP to next:      161

Set level back to 1 reverts all the stats

Details for Ranger: Quandary Crow
HP:  120
Str: 5
Dex: 10
Int: 2
Lvl: 1
Equipped with:   
Damage of attack: 0
XP to next:      100
```

</td>
</tr>
</tbody>
</table>

<br>

* Create a _MAGE_, a _Weapon_ and reflect the effects of _**changes in that Weapon**_
<table>
<thead>
<tr>
<td align="center">Description</td>
<td align="center">Sample code</td>
<td align="center">Sample output</td>
</tr>
</thead>
<tbody>
<tr>
<td>

Let's explore now the equipment and how it interacts with a character.

For this example we'll create a _Mage_. Like all characters, it starts at _Level 1_ with initial attributes for its type:
- HP: 100
- Str: 2
- Dex: 3
- Int: 10

Then, let's create a _Weapon_ - _Melee_. It also starts with _Level 1_ and has the following stats:
- Damage: 15

Now, the intricate part is the fact that the effective damage of a _Weapon_ is influenced by several factors - by its _Level_ and by the attributes of a character holding it. What is more, the attributes that have effect depend on _Weapon type_. In this case, _Melee_ has a base damage of 15, it may change by 2 points with each change in its level and the character adds 1.5 times its _Strength_  (rounded down).

So, in this case, by itself the _Melee_ has a standalone damage of 15, but when the _Mage_ gets equiped with it, the damage it can cause is 18 (15 + 1.5 * 2).

And if we change the _Melee's_ _Level_ to _3_, that will add another 4 points (2 * 2) for a total damage of 22 by the _Mage_.

_NB_: notice that if we remove the _Melee_, the stats are correctly reverted

</td>
<td>

```java
String mageName = "Qruzax";
System.out.println("Create a mage " + mageName + "\n");
Hero mage = heroFactory.createHero(HeroType.MAGE, mageName);
mage.printStats();
System.out.println("-----------------------------------\n");

String meleeName = "Dark Satchel";
System.out.println("Create a melee weapon "
     + meleeName + "\n");
Item melee = new Weapon.Builder()
        .setName(meleeName)
        .setItemType(ItemType.WEAPON)
        .setItemSubType(ItemSubType.MELEE)
        .build();
melee.calculateEffect();
mage.addItem(melee);
System.out.println("Add " + melee.getName() + " to "
     + mage.getName() + "\n");
mage.printStats();
System.out.println("-----------------------------------\n");

int level = 3;
melee.setLevel(level);
System.out.println("Change the level to " + level
     + " for " + melee.getName() + "\n");
mage.printStats();
System.out.println("-----------------------------------\n");

mage.removeItem(melee);
System.out.println("Removing " + melee.getName() + " from "
     + mage.getName() + " reverts all the stats\n");
mage.printStats();
```

</td>
<td>

```java
Create a mage Qruzax

Details for Mage: Qruzax
HP:  100
Str: 2
Dex: 3
Int: 10
Lvl: 1
Equipped with:   
Damage of attack: 0
XP to next:      100

-----------------------------------

Create a melee weapon Dark Satchel

Add Dark Satchel to Qruzax

Details for Mage: Qruzax
HP:  100
Str: 2
Dex: 3
Int: 10
Lvl: 1
Equipped with:   
             Weapon (Melee)
Damage of attack: 18
XP to next:      100

Item stats for: Dark Satchel
Item type: Weapon
Item subtype: Melee
Item level: 1
Damage: 15

-----------------------------------

Change the level to 3 for Dark Satchel

Details for Mage: Qruzax
HP:  100
Str: 2
Dex: 3
Int: 10
Lvl: 1
Equipped with:   
             Weapon (Melee)
Damage of attack: 22
XP to next:      100

Item stats for: Dark Satchel
Item type: Weapon
Item subtype: Melee
Item level: 3
Damage: 19

-----------------------------------

Removing Dark Satchel from Qruzax
            reverts all the stats

Details for Mage: Qruzax
HP:  100
Str: 2
Dex: 3
Int: 10
Lvl: 1
Equipped with:   
Damage of attack: 0
XP to next:      100
```

</td>
</tr>
</tbody>
</table>

<br>

* Create _MAGE_, an _Armor_ and reflect the effects of _**changes in that Armor**_
<table>
<thead>
<tr>
<td align="center">Description</td>
<td align="center">Sample code</td>
<td align="center">Sample output</td>
</tr>
</thead>
<tbody>
<tr>
<td>

For the last example, we're going to explore similar interaction, but now with an _Armor_.

We'll create the same _Mage_ starting at _Level 1_ with its initial attributes:
- HP: 100
- Str: 2
- Dex: 3
- Int: 10

Now, let's create an _Armor_ - _Cloth_. It starts with _Level 1_ and has the following stats:
- Slot: BODY
- Bonus Dex:  1
- Bonus Int:  3
- Bonus HP:  10

For _Armor_, there are even more factors to consider than for _Weapon_. First of all, the _Type_ of _Armor_ affects different attributes of a character wearing it and by different amounts. In this case, the _Cloth_ by itself adds 10 HP, 3 Int and 1 Dex point to Mage. However, the location of where the _Armor_ is worn also has a factor. Here, the _Cloth_ is worn on the _BODY_ and that brings 80% more points (again, rounded down), so, 18 HP, 5 Int and 1 Dex in total:
- Slot: BODY        -> 80%
- Bonus Dex:  1     -> 1
- Bonus Int:  3     -> 5
- Bonus HP:  10     -> 18

Once the _Mage_ puts the _Cloth_ on its body, it's stats are recalculated accordingly:
- HP:  118
- Str: 2
- Dex: 4
- Int: 15

Furthermore, if we change the _Level_ for _CLoth_ to _3_, the stats are further affected. Namely, each change in _Level_ of _Cloth_ adds 5 HP, 2 Int and 1 Dex. Then, accounting for placement, this results in:
- Slot: BODY            -> 80%
- Bonus Dex:  1 -> 3    -> 5
- Bonus Int:  3 -> 7    -> 12 
- Bonus HP:  10 -> 20   -> 36

This, in turn, reflects on the _Mage's_ attributes:
- HP:  154
- Str: 2
- Dex: 9
- Int: 27

One can also observe appropiate adjustments, should the _Mage_ place the _Cloth_ on another _Slot_.

_NB_: notice that if we remove the _Cloth_, the stats are correctly reverted

</td>
<td>

```java
String mageName = "Qruzax";
System.out.println("Create a mage " + mageName + "\n");
Hero mage = heroFactory.createHero(HeroType.MAGE, mageName);
mage.printStats();
System.out.println("-----------------------------------\n");

String clothName = "Cloak of Dark Worlds";
System.out.println("Create a cloth armor "
     + clothName + "\n");
Item cloth = new Armor.Builder()
        .setName(clothName)
        .setItemType(ItemType.ARMOR)
        .setItemSubType(ItemSubType.CLOTH)
        .setSlotTypeForPlacingArmor(
            SlotTypeForPlacingArmor.BODY
        )
        .build();
cloth.calculateEffect();
mage.addItem(cloth);
System.out.println("Add " + cloth.getName() + " to "
     + mage.getName() + "\n");
mage.printStats();
System.out.println("-----------------------------------\n");

int level = 3;
cloth.setLevel(level);
System.out.println("Change the level to " + level + " for "
     + cloth.getName() + "\n");
mage.printStats();
System.out.println("-----------------------------------\n");

SlotTypeForPlacingArmor head = SlotTypeForPlacingArmor.HEAD;
((Armor) cloth).setSlotTypeForPlacingArmor(head);
System.out.println("Change the slot to " + head.toString()
    .toLowerCase() + " for " + cloth.getName() + "\n");
mage.printStats();
System.out.println("-----------------------------------\n");

mage.removeItem(cloth);
System.out.println("Removing " + cloth.getName() + " from "
     + mage.getName() + " reverts all the stats\n");
mage.printStats();
```

</td>
<td>

```java
Create a mage Qruzax

Details for Mage: Qruzax
HP:  100
Str: 2
Dex: 3
Int: 10
Lvl: 1
Equipped with:   
Damage of attack: 0
XP to next:      100

-----------------------------------

Create a cloth armor Cloak of Dark Worlds

Add Cloak of Dark Worlds to Qruzax

Details for Mage: Qruzax
HP:  118
Str: 2
Dex: 4
Int: 15
Lvl: 1
Equipped with:   
              Armor (Cloth)
Damage of attack: 0
XP to next:      100

Item stats for: Cloak of Dark Worlds
Item type: Armor
Item subtype: Cloth
Item level: 1
Slot: BODY
Bonus Dex:  1
Bonus Int:  5
Bonus HP:  18

-----------------------------------

Change the level to 3 for Cloak of Dark Worlds

Details for Mage: Qruzax
HP:  154
Str: 2
Dex: 9
Int: 27
Lvl: 1
Equipped with:   
              Armor (Cloth)
Damage of attack: 0
XP to next:      100

Item stats for: Cloak of Dark Worlds
Item type: Armor
Item subtype: Cloth
Item level: 3
Slot: BODY
Bonus Dex:  5
Bonus Int:  12
Bonus HP:  36

-----------------------------------

Change the slot to head for Cloak of Dark Worlds

Details for Mage: Qruzax
HP:  194
Str: 2
Dex: 15
Int: 41
Lvl: 1
Equipped with:   
              Armor (Cloth)
Damage of attack: 0
XP to next:      100

Item stats for: Cloak of Dark Worlds
Item type: Armor
Item subtype: Cloth
Item level: 3
Slot: HEAD
Bonus Dex:  6
Bonus Int:  14
Bonus HP:  40

-----------------------------------

Removing Cloak of Dark Worlds from Qruzax
                    reverts all the stats

Details for Mage: Qruzax
HP:  100
Str: 2
Dex: 3
Int: 10
Lvl: 1
Equipped with:   
Damage of attack: 0
XP to next:      100
```

</td>
</tr>
</tbody>
</table>
