package factory;

import constants.enums.hero.HeroType;
import heroes.Hero;
import heroes.Mage;
import heroes.Ranger;
import heroes.Warrior;

public class HeroFactory {

    public Hero createHero(HeroType heroType, String name) {
        switch (heroType) {
            case WARRIOR -> {return new Warrior(name);}
            case RANGER -> {return new Ranger(name);}
            case MAGE -> {return new Mage(name);}
            default -> {return null;}
        }
    }
}
