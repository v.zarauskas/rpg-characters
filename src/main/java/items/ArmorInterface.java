package items;

import constants.enums.hero.HeroAttributeType;
import constants.enums.item.SlotTypeForPlacingArmor;
import heroes.ArmorChangesObserver;

import java.util.Map;

public interface ArmorInterface {
    Map<HeroAttributeType, Integer> getArmorBonusMapOnHeroAttributes();
    SlotTypeForPlacingArmor getSlotTypeForPlacingArmor();
    void setSlotTypeForPlacingArmor(SlotTypeForPlacingArmor slotTypeForPlacingArmor);
    void addArmorChangesObserver(ArmorChangesObserver armorChangesObserver);
    void removeArmorChangesObserver(ArmorChangesObserver armorChangesObserver);
}
