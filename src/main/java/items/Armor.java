package items;

import constants.application.wide.abbreviations.HeroAttributeAbbreviations;
import constants.enums.hero.HeroAttributeType;
import constants.enums.item.ItemSubType;
import constants.enums.item.ItemType;
import constants.enums.item.SlotTypeForPlacingArmor;
import heroes.ArmorChangesObserver;

import java.util.*;

public class Armor extends Item implements ArmorInterface {
    private static final int NUMBER_OF_ARMOR_LEVELS = 20;
    private static final int BASE_CLOTH_BONUS_ON_HEALTH = 10;
    private static final int LEVEL_CLOTH_BONUS_ON_HEALTH = 5;
    private static final int BASE_CLOTH_BONUS_ON_INTELLIGENCE = 3;
    private static final int LEVEL_CLOTH_BONUS_ON_INTELLIGENCE = 2;
    private static final int BASE_CLOTH_BONUS_ON_DEXTERITY = 1;
    private static final int LEVEL_CLOTH_BONUS_ON_DEXTERITY = 1;
    private static final int BASE_LEATHER_BONUS_ON_HEALTH = 20;
    private static final int LEVEL_LEATHER_BONUS_ON_HEALTH = 8;
    private static final int BASE_LEATHER_BONUS_ON_DEXTERITY = 3;
    private static final int LEVEL_LEATHER_BONUS_ON_DEXTERITY = 2;
    private static final int BASE_LEATHER_BONUS_ON_STRENGTH = 1;
    private static final int LEVEL_LEATHER_BONUS_ON_STRENGTH = 1;
    private static final int BASE_PLATE_BONUS_ON_HEALTH = 30;
    private static final int LEVEL_PLATE_BONUS_ON_HEALTH = 12;
    private static final int BASE_PLATE_BONUS_ON_STRENGTH = 3;
    private static final int LEVEL_PLATE_BONUS_ON_STRENGTH = 2;
    private static final int BASE_PLATE_BONUS_ON_DEXTERITY = 1;
    private static final int LEVEL_PLATE_BONUS_ON_DEXTERITY = 1;
    private static final double HEAD_SCALING_FACTOR_ON_BONUS = 2.0;
    private static final double BODY_SCALING_FACTOR_ON_BONUS = 1.8;
    private static final double LEGS_SCALING_FACTOR_ON_BONUS = 1.6;

    // A map structure to holds the armor's state: it shows the effect of the armor on each of the hero's attributes
    protected Map<HeroAttributeType, Integer> armorBonusMapOnHeroAttributes;
    protected SlotTypeForPlacingArmor slotTypeForPlacingArmor;
    protected List<ArmorChangesObserver> armorChangesObservers;

    private Armor() {
        super();
        armorBonusMapOnHeroAttributes = new HashMap<>();
        armorChangesObservers = new ArrayList<>();
    }

    public static class Builder {
        private String name;
        private ItemType itemType;
        private ItemSubType itemSubType;

        private SlotTypeForPlacingArmor slotTypeForPlacingArmor;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setItemType(ItemType itemType) {
            this.itemType = itemType;
            return this;
        }

        public Builder setItemSubType(ItemSubType itemSubType) {
            this.itemSubType = itemSubType;
            return this;
        }

        public Builder setSlotTypeForPlacingArmor(SlotTypeForPlacingArmor slotTypeForPlacingArmor) {
            this.slotTypeForPlacingArmor = slotTypeForPlacingArmor;
            return this;
        }

        public Armor build() {
            Armor armor = new Armor();
            armor.name = this.name;
            armor.itemType = this.itemType;
            armor.itemSubType = this.itemSubType;
            armor.slotTypeForPlacingArmor = this.slotTypeForPlacingArmor;

            return armor;
        }
    }

    @Override
    public void printStats() {
        StringBuilder build = super.printStatsCommonPart();
        build.append(String.format("%s", "Slot: ")).append(this.slotTypeForPlacingArmor.toString()).append("\n");
        armorBonusMapOnHeroAttributes.keySet().forEach(attribute -> {
            switch (attribute) {
                case HEALTH -> build.append(String.format("Bonus %s ", HeroAttributeAbbreviations.ABBREV_FOR_HEALTH))
                                    .append(armorBonusMapOnHeroAttributes.get(attribute)).append("\n");
                case STRENGTH -> build.append(String.format("Bonus %s ", HeroAttributeAbbreviations.ABBREV_FOR_STRENGTH))
                                    .append(armorBonusMapOnHeroAttributes.get(attribute)).append("\n");
                case DEXTERITY -> build.append(String.format("Bonus %s ", HeroAttributeAbbreviations.ABBREV_FOR_DEXTERITY))
                                    .append(armorBonusMapOnHeroAttributes.get(attribute)).append("\n");
                case INTELLIGENCE -> build.append(String.format("Bonus %s ", HeroAttributeAbbreviations.ABBREV_FOR_INTELLIGENCE))
                                    .append(armorBonusMapOnHeroAttributes.get(attribute)).append("\n");
            }
        });

        System.out.println(build.toString());
    }

    @Override
    public void calculateEffect() {
        switch (this.itemSubType) {
            case CLOTH -> updateArmorBonusMapOnHeroAttributesForCloth();
            case LEATHER -> updateArmorBonusMapOnHeroAttributesForLeather();
            case PLATE -> updateArmorBonusMapOnHeroAttributesForPlate();
        }
    }

    private void updateArmorBonusMapOnHeroAttributesForCloth() {
        Arrays.stream(HeroAttributeType.values()).forEach(attribute -> {switch (attribute) {

            case HEALTH -> {
                int standaloneClothBonusOnHealth = getStandaloneClothBonusOnHealth();
                updateArmorBonusMapOnHeroAttributes(attribute, this.slotTypeForPlacingArmor, standaloneClothBonusOnHealth);
            }

            case INTELLIGENCE -> {
                int standaloneClothBonusOnIntelligence = getStandaloneClothBonusOnIntelligence();
                updateArmorBonusMapOnHeroAttributes(attribute, this.slotTypeForPlacingArmor, standaloneClothBonusOnIntelligence);
            }

            case DEXTERITY -> {
                int standaloneClothBonusOnDexterity = getStandaloneClothBonusOnDexterity();
                updateArmorBonusMapOnHeroAttributes(attribute, this.slotTypeForPlacingArmor, standaloneClothBonusOnDexterity);
            }
        }});
    }

    private void updateArmorBonusMapOnHeroAttributesForLeather() {
        Arrays.stream(HeroAttributeType.values()).forEach(attribute -> {switch (attribute) {

            case HEALTH -> {
                int standaloneLeatherBonusOnHealth = getStandaloneLeatherBonusOnHealth();
                updateArmorBonusMapOnHeroAttributes(attribute, this.slotTypeForPlacingArmor, standaloneLeatherBonusOnHealth);
            }

            case DEXTERITY -> {
                int standaloneLeatherBonusOnDexterity = getStandaloneLeatherBonusOnDexterity();
                updateArmorBonusMapOnHeroAttributes(attribute, this.slotTypeForPlacingArmor, standaloneLeatherBonusOnDexterity);
            }

            case STRENGTH -> {
                int standaloneLeatherBonusOnStrength = getStandaloneLeatherBonusOnStrength();
                updateArmorBonusMapOnHeroAttributes(attribute, this.slotTypeForPlacingArmor, standaloneLeatherBonusOnStrength);
            }
        }});
    }

    private void updateArmorBonusMapOnHeroAttributesForPlate() {
        Arrays.stream(HeroAttributeType.values()).forEach(attribute -> {switch (attribute) {

            case HEALTH -> {
                int standalonePlateBonusOnHealth = getStandalonePlateBonusOnHealth();
                updateArmorBonusMapOnHeroAttributes(attribute, this.slotTypeForPlacingArmor, standalonePlateBonusOnHealth);
            }

            case STRENGTH -> {
                int standalonePlateBonusOnStrength = getStandalonePlateBonusOnStrength();
                updateArmorBonusMapOnHeroAttributes(attribute, this.slotTypeForPlacingArmor, standalonePlateBonusOnStrength);
            }

            case DEXTERITY -> {
                int standalonePlateBonusOnDexterity = getStandalonePlateBonusOnDexterity();
                updateArmorBonusMapOnHeroAttributes(attribute, this.slotTypeForPlacingArmor, standalonePlateBonusOnDexterity);
            }
        }});
    }

    private int getStandaloneClothBonusOnHealth() {
        return BASE_CLOTH_BONUS_ON_HEALTH + (this.level - 1) * LEVEL_CLOTH_BONUS_ON_HEALTH;
    }

    private int getStandaloneClothBonusOnIntelligence() {
        return BASE_CLOTH_BONUS_ON_INTELLIGENCE + (this.level - 1) * LEVEL_CLOTH_BONUS_ON_INTELLIGENCE;
    }

    private int getStandaloneClothBonusOnDexterity() {
        return BASE_CLOTH_BONUS_ON_DEXTERITY + (this.level - 1) * LEVEL_CLOTH_BONUS_ON_DEXTERITY;
    }

    private int getStandaloneLeatherBonusOnHealth() {
        return BASE_LEATHER_BONUS_ON_HEALTH + (this.level - 1) * LEVEL_LEATHER_BONUS_ON_HEALTH;
    }

    private int getStandaloneLeatherBonusOnDexterity() {
        return BASE_LEATHER_BONUS_ON_DEXTERITY + (this.level - 1) * LEVEL_LEATHER_BONUS_ON_DEXTERITY;
    }

    private int getStandaloneLeatherBonusOnStrength() {
        return BASE_LEATHER_BONUS_ON_STRENGTH + (this.level - 1) * LEVEL_LEATHER_BONUS_ON_STRENGTH;
    }

    private int getStandalonePlateBonusOnHealth() {
        return BASE_PLATE_BONUS_ON_HEALTH + (this.level - 1) * LEVEL_PLATE_BONUS_ON_HEALTH;
    }

    private int getStandalonePlateBonusOnStrength() {
        return BASE_PLATE_BONUS_ON_STRENGTH + (this.level - 1) * LEVEL_PLATE_BONUS_ON_STRENGTH;
    }

    private int getStandalonePlateBonusOnDexterity() {
        return BASE_PLATE_BONUS_ON_DEXTERITY + (this.level - 1) * LEVEL_PLATE_BONUS_ON_DEXTERITY;
    }

    private void updateArmorBonusMapOnHeroAttributes(HeroAttributeType attribute, SlotTypeForPlacingArmor slot, int standaloneArmorTypeBonusOnHeroAttribute) {
        int armorTypeBonusOnHeroAttribute = switch (slot) {
            case HEAD -> (int) (HEAD_SCALING_FACTOR_ON_BONUS * standaloneArmorTypeBonusOnHeroAttribute);
            case BODY -> (int) (BODY_SCALING_FACTOR_ON_BONUS * standaloneArmorTypeBonusOnHeroAttribute);
            case LEGS -> (int) (LEGS_SCALING_FACTOR_ON_BONUS * standaloneArmorTypeBonusOnHeroAttribute);
        };
        armorBonusMapOnHeroAttributes.put(attribute, armorTypeBonusOnHeroAttribute);
    }

    @Override
    public Map<HeroAttributeType, Integer> getArmorBonusMapOnHeroAttributes() {
        return armorBonusMapOnHeroAttributes;
    }

    @Override
    public SlotTypeForPlacingArmor getSlotTypeForPlacingArmor() {
        return this.slotTypeForPlacingArmor;
    }

    @Override
    public void setSlotTypeForPlacingArmor(SlotTypeForPlacingArmor slotTypeForPlacingArmor) {
        this.slotTypeForPlacingArmor = slotTypeForPlacingArmor;
        calculateEffect();
        notifyArmorChangesObservers();
    }

    @Override
    public void setLevel(int level) {
        if (level < 1 || level > NUMBER_OF_ARMOR_LEVELS) {
            System.out.println("The number of armor levels in the game is " + NUMBER_OF_ARMOR_LEVELS + ".");
            System.out.println("The input number " + level + " is not valid.");
        } else {
            this.level = level;
            calculateEffect();
            notifyArmorChangesObservers();
        }
    }

    @Override
    public void addArmorChangesObserver(ArmorChangesObserver armorChangesObserver) {
        armorChangesObservers.add(armorChangesObserver);
    }

    @Override
    public void removeArmorChangesObserver(ArmorChangesObserver armorChangesObserver) {
        armorChangesObservers.remove(armorChangesObserver);
    }

    private void notifyArmorChangesObservers() {
        for (ArmorChangesObserver observer : armorChangesObservers) {
            observer.updateArmorChangesObservers(this);
        }
    }
}
