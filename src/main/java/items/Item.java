package items;

import constants.enums.item.ItemSubType;
import constants.enums.item.ItemType;

public abstract class Item implements ItemInterface {
    protected String name;
    protected ItemType itemType;
    protected ItemSubType itemSubType;
    protected int level;

    public Item() {
        this.level = 1;
    }

    @Override
    public abstract void printStats();

    @Override
    public ItemType getItemType() {
        return this.itemType;
    }

    @Override
    public ItemSubType getItemSubType() {
        return this.itemSubType;
    }

    @Override
    public String getItemTypeString() {
        return itemSubType.getItemTypeString();
    }

    @Override
    public String getItemSubTypeString() {
        return itemSubType.getItemSubtypeString();
    }

    @Override
    public int getLevel() {
        return this.level;
    }

    @Override
    public abstract void setLevel(int level);

    protected StringBuilder printStatsCommonPart() {
        StringBuilder build = new StringBuilder();

        build.append("Item stats for: ").append(this.name).append("\n");
        build.append(String.format("%s", "Item type: ")).append(this.itemSubType.getItemTypeString()).append("\n");
        build.append(String.format("%s", "Item subtype: ")).append(this.itemSubType.getItemSubtypeString()).append("\n");
        build.append(String.format("%s", "Item level: ")).append(this.level).append("\n");
        return build;
    }

    public String getName() {
        return name;
    }
}
