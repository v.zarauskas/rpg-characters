package items;

import constants.enums.item.ItemSubType;
import constants.enums.item.ItemType;

public interface ItemInterface {
    void calculateEffect();
    void printStats();
    ItemType getItemType();
    ItemSubType getItemSubType();
    String getItemTypeString();
    String getItemSubTypeString();
    int getLevel();
    void setLevel(int level);
}
