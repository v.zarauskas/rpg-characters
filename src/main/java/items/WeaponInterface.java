package items;

import heroes.WeaponChangesObserver;

public interface WeaponInterface {
    int getWeaponDamage();
    void addWeaponChangesObserver(WeaponChangesObserver weaponChangesObserver);
    void removeWeaponChangesObserver(WeaponChangesObserver weaponChangesObserver);
}
