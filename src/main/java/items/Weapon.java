package items;

import constants.enums.item.ItemSubType;
import constants.enums.item.ItemType;
import heroes.WeaponChangesObserver;

import java.util.ArrayList;
import java.util.List;

public class Weapon extends Item implements WeaponInterface {
    private static final int NUMBER_OF_WEAPON_LEVELS = 20;
    private static final int BASE_MELEE_DAMAGE = 15;
    private static final int LEVEL_MELEE_DAMAGE = 2;
    private static final int BASE_RANGE_DAMAGE = 5;
    private static final int LEVEL_RANGE_DAMAGE = 3;
    private static final int BASE_MAGIC_DAMAGE = 25;
    private static final int LEVEL_MAGIC_DAMAGE = 2;

    protected int weaponDamage;
    protected List<WeaponChangesObserver> weaponChangesObservers;

    private Weapon() {
        super();
        this.weaponChangesObservers = new ArrayList<>();
    }

    public static class Builder {
        private String name;
        private ItemType itemType;
        private ItemSubType itemSubType;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setItemType(ItemType itemType) {
            this.itemType = itemType;
            return this;
        }

        public Builder setItemSubType(ItemSubType itemSubType) {
            this.itemSubType = itemSubType;
            return this;
        }

        public Weapon build() {
            Weapon weapon = new Weapon();
            weapon.name = this.name;
            weapon.itemType = this.itemType;
            weapon.itemSubType = this.itemSubType;

            return weapon;
        }
    }

    @Override
    public void printStats() {
        StringBuilder build = super.printStatsCommonPart();
        build.append(String.format("%s", "Damage: ")).append(this.weaponDamage).append("\n");

        System.out.println(build.toString());
    }

    @Override
    public void calculateEffect() {
        this.weaponDamage = switch (this.itemSubType) {
            case MELEE -> BASE_MELEE_DAMAGE + LEVEL_MELEE_DAMAGE * (this.level - 1);
            case RANGE -> BASE_RANGE_DAMAGE + LEVEL_RANGE_DAMAGE * (this.level - 1);
            case MAGIC -> BASE_MAGIC_DAMAGE + LEVEL_MAGIC_DAMAGE * (this.level - 1);
            default -> 0;
        };
    }

    @Override
    public int getWeaponDamage() {
        return this.weaponDamage;
    }

    @Override
    public void setLevel(int level) {
        if (level < 1 || level > NUMBER_OF_WEAPON_LEVELS) {
            System.out.println("The number of weapon levels in the game is " + NUMBER_OF_WEAPON_LEVELS + ".");
            System.out.println("The input number " + level + " is not valid.");
        } else {
            this.level = level;
            calculateEffect();
            notifyWeaponChangesObservers();
        }
    }

    @Override
    public void addWeaponChangesObserver(WeaponChangesObserver weaponChangesObserver) {
        weaponChangesObservers.add(weaponChangesObserver);
    }

    @Override
    public void removeWeaponChangesObserver(WeaponChangesObserver weaponChangesObserver) {
        weaponChangesObservers.remove(weaponChangesObserver);
    }

    private void notifyWeaponChangesObservers() {
        for (WeaponChangesObserver observer : weaponChangesObservers) {
            observer.updateWeaponChangesObservers(this);
        }
    }
}
