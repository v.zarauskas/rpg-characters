import constants.enums.hero.HeroType;
import constants.enums.item.ItemSubType;
import constants.enums.item.ItemType;
import constants.enums.item.SlotTypeForPlacingArmor;
import factory.HeroFactory;
import heroes.Hero;
import items.Armor;
import items.Item;
import items.Weapon;

public class RPGCharacters {

    public static void main(String[] args) {

        HeroFactory heroFactory = new HeroFactory();

//        warriorWithChangeInXp(heroFactory);
//        rangerWithChangeInLevel(heroFactory);
//        mageWithDifferentItems(heroFactory);
//        mageWithWeaponAndChangesInWeapon(heroFactory);
        mageWithArmorAndChangesInArmor(heroFactory);
    }

    private static void warriorWithChangeInXp(HeroFactory heroFactory) {
        // Create a WARRIOR and reflect the effect of a change in XP
        String warriorName = "Mammothbolt";
        System.out.println("Create a warrior " + warriorName + "\n");
        Hero warrior = heroFactory.createHero(HeroType.WARRIOR, warriorName);
        warrior.printStats();

        int xp = 343;
        warrior.setXp(xp);
        System.out.println("Set XP to " + xp + "\n");
        warrior.printStats();

        int xpRevert = 0;
        warrior.setXp(xpRevert);
        System.out.println("Set XP to " + xpRevert + " reverts all the stats\n");
        warrior.printStats();
        System.out.println("---------------------------------------------\n");
    }

    private static void rangerWithChangeInLevel(HeroFactory heroFactory) {
        // Create a RANGER and reflect the effect of a change in level
        String rangerName = "Quandary Crow";
        System.out.println("Create a ranger " + rangerName + "\n");
        Hero ranger = heroFactory.createHero(HeroType.RANGER, rangerName);
        ranger.printStats();

        int level = 6;
        ranger.setLevel(level);
        System.out.println("Set level to " + level + "\n");
        ranger.printStats();

        int levelRevert = 1;
        ranger.setLevel(levelRevert);
        System.out.println("Set level back to " + levelRevert + " reverts all the stats\n");
        ranger.printStats();
        System.out.println("---------------------------------------------\n");
    }

    private static void mageWithDifferentItems(HeroFactory heroFactory) {
        // Create a MAGE, different items and reflect the effects of changes
        String mageName = "Qruzax";
        System.out.println("Create a mage " + mageName + "\n");
        Hero mage = heroFactory.createHero(HeroType.MAGE, mageName);
        mage.printStats();
        System.out.println("---------------------------------------------\n");

        String meleeName = "Dark Satchel";
        System.out.println("Create a melee weapon " + meleeName + "\n");
        Item melee = new Weapon.Builder()
                .setName(meleeName)
                .setItemType(ItemType.WEAPON)
                .setItemSubType(ItemSubType.MELEE)
                .build();
        melee.calculateEffect();
        mage.addItem(melee);
        System.out.println("Add " + melee.getName() + " to " + mage.getName() + "\n");
        mage.printStats();
        System.out.println("---------------------------------------------\n");

        String rangeName = "Chaossong, Instrument of Lost Comrades";
        System.out.println("Create a range weapon " + rangeName + "\n");
        Item range = new Weapon.Builder()
                .setName(rangeName)
                .setItemType(ItemType.WEAPON)
                .setItemSubType(ItemSubType.RANGE)
                .build();
        range.calculateEffect();
        mage.addItem(range);
        System.out.println("Add " + range.getName() + " to " + mage.getName() + "\n");
        mage.printStats();
        System.out.println("---------------------------------------------\n");

        String magicName = "Black Sorcery";
        System.out.println("Create a magic weapon " + magicName + "\n");
        Item magic = new Weapon.Builder()
                .setName(mageName)
                .setItemType(ItemType.WEAPON)
                .setItemSubType(ItemSubType.MAGIC)
                .build();
        magic.calculateEffect();
        mage.addItem(magic);
        System.out.println("Add " + magic.getName() + " to " + mage.getName() + "\n");
        mage.printStats();
        System.out.println("---------------------------------------------\n");

        String clothName = "Cloak of Dark Worlds";
        System.out.println("Create a cloth armor " + clothName + "\n");
        Item cloth = new Armor.Builder()
                .setName(clothName)
                .setItemType(ItemType.ARMOR)
                .setItemSubType(ItemSubType.CLOTH)
                .setSlotTypeForPlacingArmor(SlotTypeForPlacingArmor.BODY)
                .build();
        cloth.calculateEffect();
        mage.addItem(cloth);
        System.out.println("Add " + cloth.getName() + " to " + mage.getName() + "\n");
        mage.printStats();
        System.out.println("---------------------------------------------\n");

        String leatherName = "Mind's Eye";
        System.out.println("Create a leather armor " + leatherName + "\n");
        Item leather = new Armor.Builder()
                .setName(leatherName)
                .setItemType(ItemType.ARMOR)
                .setItemSubType(ItemSubType.LEATHER)
                .setSlotTypeForPlacingArmor(SlotTypeForPlacingArmor.LEGS)
                .build();
        leather.calculateEffect();
        mage.addItem(leather);
        System.out.println("Add " + leather.getName() + " to " + mage.getName() + "\n");
        mage.printStats();
        System.out.println("---------------------------------------------\n");

        String plateName = "Bracers of Hellish Fires";
        System.out.println("Create a plate armor " + plateName + "\n");
        Item plate = new Armor.Builder()
                .setName(plateName)
                .setItemType(ItemType.ARMOR)
                .setItemSubType(ItemSubType.LEATHER)
                .setSlotTypeForPlacingArmor(SlotTypeForPlacingArmor.LEGS)
                .build();
        plate.calculateEffect();
        mage.addItem(plate);
        System.out.println("Add " + plate.getName() + " to " + mage.getName() + "\n");
        mage.printStats();
        System.out.println("---------------------------------------------\n");
    }

    private static void mageWithWeaponAndChangesInWeapon(HeroFactory heroFactory) {
        // Create a MAGE, a weapon and reflect the effects of changes in that weapon
        String mageName = "Qruzax";
        System.out.println("Create a mage " + mageName + "\n");
        Hero mage = heroFactory.createHero(HeroType.MAGE, mageName);
        mage.printStats();
        System.out.println("---------------------------------------------\n");

        String meleeName = "Dark Satchel";
        System.out.println("Create a melee weapon " + meleeName + "\n");
        Item melee = new Weapon.Builder()
                .setName(meleeName)
                .setItemType(ItemType.WEAPON)
                .setItemSubType(ItemSubType.MELEE)
                .build();
        melee.calculateEffect();
        mage.addItem(melee);
        System.out.println("Add " + melee.getName() + " to " + mage.getName() + "\n");
        mage.printStats();
        System.out.println("---------------------------------------------\n");

        int level = 3;
        melee.setLevel(level);
        System.out.println("Change the level to " + level + " for " + melee.getName() + "\n");
        mage.printStats();
        System.out.println("---------------------------------------------\n");

        mage.removeItem(melee);
        System.out.println("Removing " + melee.getName() + " from " + mage.getName() + " reverts all the stats\n");
        mage.printStats();
        System.out.println("---------------------------------------------\n");

    }

    private static void mageWithArmorAndChangesInArmor(HeroFactory heroFactory) {
        // Create a MAGE, a weapon and reflect the effects of changes in that weapon
        String mageName = "Qruzax";
        System.out.println("Create a mage " + mageName + "\n");
        Hero mage = heroFactory.createHero(HeroType.MAGE, mageName);
        mage.printStats();
        System.out.println("---------------------------------------------\n");

        String clothName = "Cloak of Dark Worlds";
        System.out.println("Create a cloth armor " + clothName + "\n");
        Item cloth = new Armor.Builder()
                .setName(clothName)
                .setItemType(ItemType.ARMOR)
                .setItemSubType(ItemSubType.CLOTH)
                .setSlotTypeForPlacingArmor(SlotTypeForPlacingArmor.BODY)
                .build();
        cloth.calculateEffect();
        mage.addItem(cloth);
        System.out.println("Add " + cloth.getName() + " to " + mage.getName() + "\n");
        mage.printStats();
        System.out.println("---------------------------------------------\n");

        int level = 3;
        cloth.setLevel(level);
        System.out.println("Change the level to " + level + " for " + cloth.getName() + "\n");
        mage.printStats();
        System.out.println("---------------------------------------------\n");

        SlotTypeForPlacingArmor head = SlotTypeForPlacingArmor.HEAD;
        ((Armor) cloth).setSlotTypeForPlacingArmor(head);
        System.out.println("Change the slot to " + head.toString().toLowerCase() + " for " + cloth.getName() + "\n");
        mage.printStats();
        System.out.println("---------------------------------------------\n");

        mage.removeItem(cloth);
        System.out.println("Removing " + cloth.getName() + " from " + mage.getName() + " reverts all the stats\n");
        mage.printStats();
    }
}
