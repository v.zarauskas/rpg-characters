package heroes;

import items.Armor;

public interface ArmorChangesObserver {
    void updateArmorChangesObservers(Armor armor);
}
