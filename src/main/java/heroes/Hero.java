package heroes;

import constants.application.wide.abbreviations.HeroAttributeAbbreviations;
import constants.enums.hero.HeroAttributeType;
import constants.enums.hero.HeroType;
import items.Armor;
import items.Item;
import items.Weapon;

import java.util.ArrayList;
import java.util.List;

public abstract class Hero implements HeroInterface, ArmorChangesObserver, WeaponChangesObserver {
    private static final int NUMBER_OF_LEVELS_IN_THE_GAME = 20;
    private static final int MAX_NR_OF_XP_POINTS_TO_FINISH_THE_GAME = getAbsoluteLevelXpThreshold(NUMBER_OF_LEVELS_IN_THE_GAME);
    private static final int INITIAL_AMOUNT_OF_XP_TO_CHANGE_LEVEL = 100;
    private static final double XP_FACTOR_TO_REACH_NEXT_LEVEL = 0.1;
    private static final double STRENGTH_FACTOR_ON_MELEE = 1.5;
    private static final double DEXTERITY_FACTOR_ON_RANGE = 2;
    private static final double INTELLIGENCE_FACTOR_ON_MAGIC = 3;
    private static final String ABBREV_FOR_LEVEL = "Lvl: ";
    private static final String ABBREV_FOR_ITEMS = "Equipped with: ";
    private static final String ABBREV_FOR_ATTACK = "Damage of attack: ";
    private static final String ABBREV_FOR_XP_TO_NEXT = "XP to next: ";

    protected String name;
    protected HeroType heroType;
    protected int health;
    protected int strength;
    protected int dexterity;
    protected int intelligence;
    protected List<Weapon> weapons;
    protected List<Armor> armor;
    protected int xp;
    protected int level;
    protected int heroAttackDamage;

    public Hero(String name) {
        this.name = name;
        this.xp = 0;
        this.level = 1;
        weapons = new ArrayList<>();
        armor = new ArrayList<>();
    }

    @Override
    public void calculateHeroAttackDamage() {
        if (!hasWeapons()) {
            this.heroAttackDamage = 0;
        }
        // calculates the sum of damages from each weapon at hero's disposal
        this.heroAttackDamage = weapons.stream().mapToInt(this::calculateDamage).sum();
    }

    @Override
    public void printStats() {
        StringBuilder build = new StringBuilder();

        build.append(String.format("Details for %s: ", this.heroType.getHeroTypeString())).append(this.name).append("\n");
        build.append(String.format("%-5s", HeroAttributeAbbreviations.ABBREV_FOR_HEALTH)).append(this.health).append("\n");
        build.append(String.format("%-5s", HeroAttributeAbbreviations.ABBREV_FOR_STRENGTH)).append(this.strength).append("\n");
        build.append(String.format("%-5s", HeroAttributeAbbreviations.ABBREV_FOR_DEXTERITY)).append(this.dexterity).append("\n");
        build.append(String.format("%-5s", HeroAttributeAbbreviations.ABBREV_FOR_INTELLIGENCE)).append(this.intelligence).append("\n");
        build.append(String.format("%-5s", ABBREV_FOR_LEVEL)).append(this.level).append("\n");
        build.append(String.format("%-17s", ABBREV_FOR_ITEMS)).append("\n");

        appendAllEquippedItemsForStats(build);

        build.append(String.format("%-17s", ABBREV_FOR_ATTACK)).append(this.heroAttackDamage).append("\n");
        build.append(String.format("%-17s", ABBREV_FOR_XP_TO_NEXT)).append(getXpToNextLevel()).append("\n");

        System.out.println(build.toString());

        printStatsForAllEquippedItems();
    }

    @Override
    public void addItem(Item item) {
        switch (item.getItemType()) {
            case WEAPON -> {
                weapons.add((Weapon) item);
                ((Weapon) item).addWeaponChangesObserver(this);
                calculateHeroAttackDamage();
            }
            case ARMOR -> {
                armor.add((Armor) item);
                ((Armor) item).addArmorChangesObserver(this);
                updateHeroStatsFromArmorBonus((Armor) item);
            }
        }
    }

    @Override
    public void removeItem(Item item) {
        switch (item.getItemType()) {
            case WEAPON -> {
                weapons.remove(item);
                ((Weapon) item).removeWeaponChangesObserver(this);
                calculateHeroAttackDamage();
            }
            case ARMOR -> {
                armor.remove(item);
                ((Armor) item).removeArmorChangesObserver(this);
                updateHeroStatsWithLevelChange();
            }
        }
    }

    @Override
    public void setXp(int xp) {
        if (xp < 0 || xp > MAX_NR_OF_XP_POINTS_TO_FINISH_THE_GAME) {
            System.out.println("The range of XP points in the game is 0 - " + MAX_NR_OF_XP_POINTS_TO_FINISH_THE_GAME);
            System.out.println("The input amount " + xp + " is not valid.");
        } else {
            this.xp = xp;
            updateLevelWithManualXpChange(xp);
            updateHeroStatsWithLevelChange();
        }
    }

    private void updateLevelWithManualXpChange(int xp) {
        if (xp < 100) {
            this.level = 1;
        } else {
            // 'NUMBER_OF_LEVELS_IN_THE_GAME + 1' below in the final iteration of the loop assumes that, at the last
            // level, it requires to gain xp points by the same factor to finish the game
            for (int level = 1; level <= NUMBER_OF_LEVELS_IN_THE_GAME; level++) {
                boolean isInRangeOfLevelXp = getAbsoluteLevelXpThreshold(level) <= xp && xp < getAbsoluteLevelXpThreshold(level + 1);
                if (isInRangeOfLevelXp) {
                    this.level = level + 1; // '+1' because 'level' refers to the threshold of the previous level
                }
            }
        }
    }

    @Override
    public void setLevel(int level) {
        if (level < 1 || level > NUMBER_OF_LEVELS_IN_THE_GAME) {
            System.out.println("The number of levels in the game is " + MAX_NR_OF_XP_POINTS_TO_FINISH_THE_GAME + ".");
            System.out.println("The input number " + level + " is not valid.");
        } else {
            this.level = level;
            updateXpWithManualLevelChange(level);
            updateHeroStatsWithLevelChange();
        }
    }

    // Assigns the lowest value of the level XP point range
    private void updateXpWithManualLevelChange(int level) {
        if (level == 1) {
            this.xp = 0;
        } else {
            this.xp = getAbsoluteLevelXpThreshold(level - 1);
        }
    }

    protected abstract void updateHeroStatsWithLevelChange();

    @Override
    public void updateArmorChangesObservers(Armor armor) {
        updateHeroStatsFromArmorBonus(armor);
    }

    private void updateHeroStatsFromArmorBonus(Armor armor) {
        Integer effectOnHealth = armor.getArmorBonusMapOnHeroAttributes().get(HeroAttributeType.HEALTH);
        Integer effectOnStrength = armor.getArmorBonusMapOnHeroAttributes().get(HeroAttributeType.STRENGTH);
        Integer effectOnDexterity = armor.getArmorBonusMapOnHeroAttributes().get(HeroAttributeType.DEXTERITY);
        Integer effectOnIntelligence = armor.getArmorBonusMapOnHeroAttributes().get(HeroAttributeType.INTELLIGENCE);

        if (effectOnHealth != null) this.health += effectOnHealth;
        if (effectOnStrength != null) this.strength += effectOnStrength;
        if (effectOnDexterity != null) this.dexterity += effectOnDexterity;
        if (effectOnIntelligence != null) this.intelligence += effectOnIntelligence;
    }

    @Override
    public void updateWeaponChangesObservers(Weapon weapon) {
        calculateHeroAttackDamage();
    }

    public String getName() {
        return name;
    }

    private boolean hasWeapons() {
        if (weapons == null) return false;
        return weapons.size() > 0;
    }

    private int calculateDamage(Weapon weapon) {
        int damage;

        int weaponDamage = weapon.getWeaponDamage();
        int heroDamageEffect = switch (weapon.getItemSubType()) {
            case MELEE -> (int) (STRENGTH_FACTOR_ON_MELEE * this.strength);
            case RANGE -> (int) (DEXTERITY_FACTOR_ON_RANGE * this.dexterity);
            case MAGIC -> (int) (INTELLIGENCE_FACTOR_ON_MAGIC * this.intelligence);
            default -> 0;
        };

        damage = weaponDamage + heroDamageEffect;

        return damage;
    }

    private void appendAllEquippedItemsForStats(StringBuilder build) {
        if (weapons != null) {
            weapons.forEach(weapon -> appendItemForStats(build, weapon));
        }
        if (armor != null) {
            armor.forEach(armor -> appendItemForStats(build, armor));
        }
    }

    private void appendItemForStats(StringBuilder build, Item item) {
        String itemType = item.getItemTypeString();
        String itemSubtype = item.getItemSubTypeString();
        build.append(String.format("%19s (%s)", itemType, itemSubtype)).append("\n");
    }

    private int getXpToNextLevel() {
        int nextLevelXpThreshold = getAbsoluteLevelXpThreshold(this.level);
        return nextLevelXpThreshold - this.xp;
    }

    private static int getAbsoluteLevelXpThreshold(int level) {
        // compounding formula (1 + rate)^(nrOfLevelsToAdvance)
        double compoundFactor = 0;
        for (int i = 1; i <= level; i++) {
            compoundFactor += Math.pow((1 + XP_FACTOR_TO_REACH_NEXT_LEVEL), (i - 1));
        }
        return (int) (INITIAL_AMOUNT_OF_XP_TO_CHANGE_LEVEL * compoundFactor);
    }

    private void printStatsForAllEquippedItems() {
        if (weapons != null) {
            weapons.forEach(Weapon::printStats);
        }
        if (armor != null) {
            armor.forEach(Armor::printStats);
        }
    }
}