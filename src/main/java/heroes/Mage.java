package heroes;

import constants.enums.hero.HeroType;

public class Mage extends Hero {
    public Mage(String name) {
        super(name);
        this.heroType = HeroType.MAGE;
        this.health = 100;
        this.strength = 2;
        this.dexterity = 3;
        this.intelligence = 10;
    }

    @Override
    protected void updateHeroStatsWithLevelChange() {
        this.health = 100 + (this.level - 1) * 15;
        this.strength = 2 + (this.level - 1);
        this.dexterity = 3 + (this.level - 1) * 2;
        this.intelligence = 10 + (this.level - 1) * 5;
    }
}
