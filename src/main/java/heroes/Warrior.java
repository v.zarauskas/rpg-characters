package heroes;

import constants.enums.hero.HeroType;

public class Warrior extends Hero{
    public Warrior(String name) {
        super(name);
        this.heroType = HeroType.WARRIOR;
        this.health = 150;
        this.strength = 10;
        this.dexterity = 3;
        this.intelligence = 1;
    }

    @Override
    protected void updateHeroStatsWithLevelChange() {
        this.health = 150 + (this.level - 1) * 30;
        this.strength = 10 + (this.level - 1) * 5;
        this.dexterity = 3 + (this.level - 1) * 2;
        this.intelligence = 1 + (this.level - 1);
    }
}
