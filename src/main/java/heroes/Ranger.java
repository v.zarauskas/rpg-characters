package heroes;

import constants.enums.hero.HeroType;

public class Ranger extends Hero{
    public Ranger(String name) {
        super(name);
        this.heroType = HeroType.RANGER;
        this.health = 120;
        this.strength = 5;
        this.dexterity = 10;
        this.intelligence = 2;
    }

    @Override
    protected void updateHeroStatsWithLevelChange() {
        this.health = 120 + (this.level - 1) * 20;
        this.strength = 5 + (this.level - 1) * 2;
        this.dexterity = 10 + (this.level - 1) * 5;
        this.intelligence = 2 + (this.level - 1);
    }
}
