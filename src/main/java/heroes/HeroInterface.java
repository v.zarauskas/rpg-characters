package heroes;

import items.Item;

public interface HeroInterface {
    void calculateHeroAttackDamage();
    void printStats();
    void addItem(Item item);
    void removeItem(Item item);
    void setXp(int xp);
    void setLevel(int level);
}
