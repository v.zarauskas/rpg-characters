package heroes;

import items.Weapon;

public interface WeaponChangesObserver {
    void updateWeaponChangesObservers(Weapon weapon);
}
