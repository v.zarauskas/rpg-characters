package constants.enums.hero;

public enum HeroAttributeType {
    HEALTH, STRENGTH, DEXTERITY, INTELLIGENCE
}
