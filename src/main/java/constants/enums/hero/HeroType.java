package constants.enums.hero;

public enum HeroType implements HeroTypeInterface {
    WARRIOR("Warrior"),
    RANGER("Ranger"),
    MAGE("Mage");

    private String type;

    HeroType(String type) {
        this.type = type;
    }

    @Override
    public String getHeroTypeString() {
        return this.type;
    }
}
