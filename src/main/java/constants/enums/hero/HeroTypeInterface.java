package constants.enums.hero;

public interface HeroTypeInterface {
    String getHeroTypeString();
}
