package constants.enums.item;

public enum ItemType implements ItemTypeInterface {
    WEAPON("Weapon"),
    ARMOR("Armor");

    private final String itemType;

    ItemType(String itemType) {
        this.itemType = itemType;
    }

    @Override
    public String getItemTypeString() {
        return this.itemType;
    }

}
