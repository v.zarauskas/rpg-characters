package constants.enums.item;

public enum ItemSubType implements ItemTypeInterface, ItemSubTypeInterface {
    MELEE("Melee", ItemType.WEAPON),
    RANGE("Range", ItemType.WEAPON),
    MAGIC("Magic", ItemType.WEAPON),
    CLOTH("Cloth", ItemType.ARMOR),
    LEATHER("Leather", ItemType.ARMOR),
    PLATE("Plate", ItemType.ARMOR);

    private final String itemSubtypeName;
    private final ItemType itemType;

    ItemSubType(String weaponName, ItemType itemType) {
        this.itemSubtypeName = weaponName;
        this.itemType = itemType;
    }

    @Override
    public String getItemTypeString() {
        return this.itemType.getItemTypeString();
    }

    @Override
    public String getItemSubtypeString() {
        return this.itemSubtypeName;
    }
}
