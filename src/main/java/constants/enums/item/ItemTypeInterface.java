package constants.enums.item;

public interface ItemTypeInterface {
    String getItemTypeString();
}
