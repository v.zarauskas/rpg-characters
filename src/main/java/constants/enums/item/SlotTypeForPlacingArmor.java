package constants.enums.item;

public enum SlotTypeForPlacingArmor {
    HEAD, BODY, LEGS
}
