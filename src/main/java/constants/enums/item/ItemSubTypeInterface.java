package constants.enums.item;

public interface ItemSubTypeInterface {
    String getItemSubtypeString();
}
