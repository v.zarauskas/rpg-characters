package constants.application.wide.abbreviations;

public class HeroAttributeAbbreviations {
    public static final String ABBREV_FOR_HEALTH = "HP: ";
    public static final String ABBREV_FOR_STRENGTH = "Str: ";
    public static final String ABBREV_FOR_DEXTERITY = "Dex: ";
    public static final String ABBREV_FOR_INTELLIGENCE = "Int: ";
}